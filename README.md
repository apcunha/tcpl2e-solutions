# TCPL 2e exercise solutions

This repository contains solutions for the exercises found on 2nd
edition of the "The C Programming Language" book (TCPL 2e), by Brian
Kernighan and Dennis Ritchie.

## Solution constraints

I try my best to constrain the solutions to the features that have thus
far been covered by the book by the time the exercise is presented.

## A word on standards

Considering TCPL 2e was released in 1988, the solutions in this
repository should use only features present in the C89 standard: there
should be no single-line `//` comments, no mixing of declarations and
code, among others.

There is, however, an exception: I have decided against allowing
implicit function declarations and implicit `int` typing.
The solutions are compiled with flags to explicitly disallow this
behaviour.

## License

Code in this repository is licensed under the [MIT License](LICENSE.md).
