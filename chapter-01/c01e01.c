/*
 * Exercise 1-1
 * Run the "hello, world" program on your system. Experiment with
 * leaving out parts of the programs, to see what error messages you
 * get.
 * 
 * --
 *
 * When one tries to compile the original code for strict C89, we get
 * the following errors:
 *   - return type defaults to ‘int’
 *   - function declaration isn’t a prototype
 *   - control reaches end of non-void function
 *
 * Fixing these makes us fully compliant with C89.
 */

#include <stdio.h>

int main(void)
{
    printf("hello, world!\n");

    return 0;
}
